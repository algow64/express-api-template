const { MongoClient } = require("mongodb");
const { dbName } = require('./configs');

// Query example mongo.db.collection('users').find().toArray()
class Mongo {
  constructor() {
    this.db = null;
  }

  async createDB() {
    const uri = 'mongodb://localhost:27017/' + dbName;

    const client = new MongoClient(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    await client.connect();
    console.log("MongoDB Connected");
    const db = client.db(dbName);

    this.db = db;
  }
}

module.exports = new Mongo();