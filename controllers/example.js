const express = require('express');

const router = express.Router();

router.get('/', async (request, response) => {
  return response.status(200).send({
    'message': 'hello world'
  })
});

router.post('/', async (request, response) => {
  return response.status(200).send({
    'message': 'hello world'
  })
});

router.put('/', async (request, response) => {
  return response.status(200).send({
    'message': 'hello world'
  })
});

router.delete('/', async (request, response) => {
  return response.status(200).send({
    'message': 'hello world'
  })
});


module.exports = router;