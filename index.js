const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongo = require('./app/mongo');

const run = async () => {
  const app = express();
  const port = process.env.PORT || 4000;

  await mongo.createDB();

  app.use(cors());
  app.options('*', cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  const controllers = require('./controllers/index');

  app.use('/example', controllers.example);

  app.listen(port, () => console.log(`Using port ${port}`));
}

run();