const mongo = require('../app/mongo');

class Example{
  constructor() {
    this.collection = mongo.db.collection('example');
  }

  async find(query={}) {
    //
  }

  async insert(data) {
    //
  }

  async update(id, data) {
    //
  }

  async delete(id) {
    //
  }
}

module.exports = new User();